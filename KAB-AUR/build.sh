#!/bin/bash
SCRIPT_DIR2="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd $SCRIPT_DIR2
destiny=$SCRIPT_DIR"/build/"
repo_name="kab"


search="yay"
git clone https://aur.archlinux.org/$search
cd $SCRIPT_DIR2/$search


repo_version=$(pacman -Si "$repo_name/$search" | awk '/^Version/ {print $3}')
local_version=$(grep -E '^pkgver=' PKGBUILD | cut -d= -f2)

if [ "$(vercmp "$repo_version" "$local_version")" -lt 0 ]; then
    # Trigger rebuild
    makepkg
    

echo "Moving created files to " $destiny
echo "#############################################################################################"
cp -r ./*.pkg.* $destiny
cp -r ./pkg/*.pkg.* $destiny

echo "Cleaning up"
echo "#############################################################################################"
echo "deleting unnecessary folders"
echo "#############################################################################################"

rm -rf pkg src

tput setaf 8
echo "#############################################################################################"
echo "###################                       build done                   ######################"
echo "#############################################################################################"
tput sgr0


else

tput setaf 8
echo "#############################################################################################"
echo "###################                   build not needed                 ######################"
echo "#############################################################################################"
tput sgr0

fi

cd $SCRIPT_DIR2

search="paru"
git clone https://aur.archlinux.org/$search
cd $SCRIPT_DIR2/$search


repo_version=$(pacman -Si "$repo_name/$search" | awk '/^Version/ {print $3}')
local_version=$(grep -E '^pkgver=' PKGBUILD | cut -d= -f2)

if [ "$(vercmp "$repo_version" "$local_version")" -lt 0 ]; then
    # Trigger rebuild
    makepkg
    

echo "Moving created files to " $destiny
echo "#############################################################################################"
cp -r ./*.pkg.* $destiny
cp -r ./pkg/*.pkg.* $destiny

echo "Cleaning up"
echo "#############################################################################################"
echo "deleting unnecessary folders"
echo "#############################################################################################"

rm -rf pkg src

tput setaf 8
echo "#############################################################################################"
echo "###################                       build done                   ######################"
echo "#############################################################################################"
tput sgr0


else

tput setaf 8
echo "#############################################################################################"
echo "###################                   build not needed                 ######################"
echo "#############################################################################################"
tput sgr0

fi

cd $SCRIPT_DIR2

search="snap-pac-grub"
git clone https://aur.archlinux.org/$search
cd $SCRIPT_DIR2/$search


repo_version=$(pacman -Si "$repo_name/$search" | awk '/^Version/ {print $3}')
local_version=$(grep -E '^pkgver=' PKGBUILD | cut -d= -f2)

if [ "$(vercmp "$repo_version" "$local_version")" -lt 0 ]; then
    # Trigger rebuild
    makepkg
    

echo "Moving created files to " $destiny
echo "#############################################################################################"
cp -r ./*.pkg.* $destiny
cp -r ./pkg/*.pkg.* $destiny

echo "Cleaning up"
echo "#############################################################################################"
echo "deleting unnecessary folders"
echo "#############################################################################################"

rm -rf pkg src

tput setaf 8
echo "#############################################################################################"
echo "###################                       build done                   ######################"
echo "#############################################################################################"
tput sgr0


else

tput setaf 8
echo "#############################################################################################"
echo "###################                   build not needed                 ######################"
echo "#############################################################################################"
tput sgr0

fi

cd $SCRIPT_DIR2

search="mkinitcpio-openswap"
git clone https://aur.archlinux.org/$search
cd $SCRIPT_DIR2/$search


repo_version=$(pacman -Si "$repo_name/$search" | awk '/^Version/ {print $3}')
local_version=$(grep -E '^pkgver=' PKGBUILD | cut -d= -f2)

if [ "$(vercmp "$repo_version" "$local_version")" -lt 0 ]; then
    # Trigger rebuild
    makepkg
    

echo "Moving created files to " $destiny
echo "#############################################################################################"
cp -r ./*.pkg.* $destiny
cp -r ./pkg/*.pkg.* $destiny

echo "Cleaning up"
echo "#############################################################################################"
echo "deleting unnecessary folders"
echo "#############################################################################################"

rm -rf pkg src

tput setaf 8
echo "#############################################################################################"
echo "###################                       build done                   ######################"
echo "#############################################################################################"
tput sgr0


else

tput setaf 8
echo "#############################################################################################"
echo "###################                   build not needed                 ######################"
echo "#############################################################################################"
tput sgr0

fi

cd $SCRIPT_DIR2
