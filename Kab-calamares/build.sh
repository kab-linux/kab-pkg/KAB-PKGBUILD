#!/bin/bash
SCRIPT_DIR2="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd $SCRIPT_DIR2

destiny=$SCRIPT_DIR"/build/"
search="KAB-calamares"

tput setaf 2
echo "#############################################################################################"
echo "#########        Let us build the package "$(basename `pwd`)
echo "#############################################################################################"
tput sgr0

updpkgsums
repo_name="kab"
pkg_name=$(grep -E '^pkgname=' PKGBUILD | cut -d= -f2)
repo_version=$(pacman -Si "$repo_name/$pkg_name" | awk '/^Version/ {print $3}')
local_version=$(grep -E '^pkgver=' PKGBUILD | cut -d= -f2)

if [ "$(vercmp "$repo_version" "$local_version")" -lt 0 ]; then
    # Trigger rebuild
    makepkg -s

echo "Moving created files to " $destiny
echo "#############################################################################################"
mv *pkg.tar.zst $destiny

echo "Cleaning up"
echo "#############################################################################################"
echo "deleting unnecessary folders"
echo "#############################################################################################"

rm -rf pkg src

tput setaf 8
echo "#############################################################################################"
echo "###################                       build done                   ######################"
echo "#############################################################################################"
tput sgr0


else

tput setaf 8
echo "#############################################################################################"
echo "###################                   build not needed                 ######################"
echo "#############################################################################################"
tput sgr0

fi
